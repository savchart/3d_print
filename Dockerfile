# Use the official Python image as the base image
FROM python:3.9

# Set environment variables for Python
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Install system dependencies
RUN apt-get update \
    && apt-get install --no-install-recommends -y \
        curl \
        build-essential \
    && rm -rf /var/lib/apt/lists/*

# Install Poetry
RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -

# Set Poetry environment variables
ENV PATH="${PATH}:/root/.poetry/bin"

# Copy only the requirements files to the Docker image
COPY pyproject.toml ./data/ ./src/ /app/

# Set the working directory
WORKDIR /app

# Install project dependencies using Poetry
RUN poetry install --no-root --no-interaction --no-ansi

# Copy the entire project to the Docker image
COPY . /app/

# Specify the command to run the application
CMD ["python", "src/image_processor/main.py"]
