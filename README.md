## Požadavky
napiš skript v Pythonu, který načte fotky ze zadané složky a rozdělí je do podsložek
využij libovolnou volně dostupnou knihovnu, která na základě obrazové detekce rozpozná tiskové chyby
skript musí být univerzální a fungovat s další sadou fotek
## Cíl úkolu
rozděl sadu fotek na OK tisky a NOK tisky
volitelně přidej třetí kategorii pro fotky, které nelze z různých důvodů označit (nekvalitní fotka, nestandardní výtisk…)

## Nepovinný bonus
označ typ tiskové chyby
vypiš % s jakou pravděpodobností je chyba detekována

## Popis řešení
Algorytmus:
1. Načtení fotek ze zadané složky
2. Filtrace fotek Laplacian filterem
3. Detekce chyb pomocí yolo
4. Metadata ob obrázku pomocí blip
5. Zobrazení chyb na obrázku
6. Rozdělení fotek do podsložek

## Doplňující informace

V složce `data` jsou uloženy fotky, které byly použity pro testování skriptu. \
V složce `output` jsou uloženy výstupy skriptu. Po spuštění skriptu se vytvoří podsložky `OK`, `NOK` a `UNKNOWN`. \
V složce `assets` jsou uloženy obrázky pro demonstraci algoritmu. A taky screenshoty z GPT requesty. 

CI pipeline pro vytvoření Docker image a push do Docker Hub. Jenom pro ukažku, jak by to mohlo fungovat.

## Requirements
write a Python script that reads photos from a specified folder and divides them into subfolders
use any freely available library that detects printing errors based on image detection
the script must be universal and work with another set of photos
## Task goal
divide the set of photos into OK prints and NOK prints
optionally add a third category for photos that cannot be marked for various reasons (poor quality photo, non-standard print...)

## Optional bonus
mark the type of printing error
list the % probability with which the error is detected

## Solution description
Algorithm:
1. Load photos from the specified folder
2. Filter the photos with Laplacian filter
3. Error detection using yolo
4. Image metadata using blip
5. Error display on image
6. Splitting photos into subfolders

## Additional information

The `data` folder contains the photos that were used to test the script. \
The `output` folder stores the output of the script. When the script is run, the `OK`, `NOK` and `UNKNOWN` subfolders are created. \
The `assets` folder stores images for demonstrating the algorithm. And screenshots of the GPT requests. 

CI pipeline to create Docker image and push to Docker Hub. Just to show how it could work.



