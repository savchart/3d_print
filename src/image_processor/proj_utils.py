import os


def get_cwd():
    return os.getcwd()


def create_folders():
    root = get_cwd()
    folders = ['output', 'output/OK', 'output/NOK', 'output/UNKNOWN']
    for folder in folders:
        if not os.path.exists(os.path.join(root, folder)):
            os.mkdir(os.path.join(root, folder))
