import cv2


def add_text_to_image(image, text, org=(200, 275)):
    font = cv2.FONT_HERSHEY_SIMPLEX
    font_scale = 12
    color = (0, 128, 255)
    thickness = 5
    image = cv2.putText(image, text, org, font, font_scale, color, thickness, cv2.LINE_AA)
    return image


def blurry_filter(image, threshold=30):
    laplacian_var = cv2.Laplacian(image, cv2.CV_64F).var()
    if laplacian_var < threshold:
        image = add_text_to_image(image, 'blurry')
        return image, True
    else:
        return image, False
