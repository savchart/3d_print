import torch
from PIL import ImageDraw
from transformers import BlipProcessor, BlipForQuestionAnswering

processor = BlipProcessor.from_pretrained("ybelkada/blip-vqa-base")
model = BlipForQuestionAnswering.from_pretrained("ybelkada/blip-vqa-base", torch_dtype=torch.float16).to("cuda")

coord = (125, 125)
color = (128, 0, 255)


def blip_inference(image):
    question = "What problem occurred during 3D printing on this picture?"
    inputs = processor(image, question, return_tensors="pt").to("cuda", torch.float16)
    out = model.generate(**inputs)

    draw = ImageDraw.Draw(image)
    answer = processor.decode(out[0], skip_special_tokens=True)

    draw.text(
        coord,
        "blip_infer: " + answer,
        color,
        font_size=100
    )

    return image
