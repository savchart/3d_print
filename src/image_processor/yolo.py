from PIL import Image, ImageDraw

from huggingface_hub import hf_hub_download
import torch

repo_id = "Javiai/3dprintfails-yolo5vs"
filename = "model_torch.pt"
model_path = hf_hub_download(repo_id=repo_id, filename=filename)
model = torch.hub.load('Ultralytics/yolov5', 'custom', model_path, verbose=False)


def yolo_v5_inference(image_path):
    image = Image.open(image_path)
    draw = ImageDraw.Draw(image)
    detections = model(image)

    categories = [
        {'name': 'error', 'color': (0, 0, 255)},
        {'name': 'extrusor', 'color': (0, 255, 0)},
        {'name': 'part', 'color': (255, 0, 0)},
        {'name': 'spaghetti', 'color': (0, 0, 255)}
    ]

    status = 'OK'

    for detection in detections.xyxy[0]:
        x1, y1, x2, y2, p, category_id = detection
        x1, y1, x2, y2, category_id = int(x1), int(y1), int(x2), int(y2), int(category_id)
        draw.rectangle(
            (x1, y1, x2, y2),
            outline=categories[category_id]['color'],
            width=10
        )
        score = "{:.2f}".format(p.item())
        draw.text(
            (x1, y1),
            f"yolo_infer: {categories[category_id]['name']} - {score}",
            categories[category_id]['color'],
            font_size=75
        )
        if categories[category_id]['name'] == 'error' or categories[category_id]['name'] == 'spaghetti':
            status = 'NOK'

    return image, status
