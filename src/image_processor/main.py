import os
import cv2

from tqdm.auto import tqdm
from yolo import yolo_v5_inference
from blip import blip_inference
from filters import blurry_filter
from proj_utils import get_cwd, create_folders

import logging

logging.basicConfig(level=logging.INFO)

project_root = get_cwd()
data_folder = os.path.join(project_root, 'data')
save_folder = os.path.join(project_root, 'output')

def main():
    logging.info('Starting the process')
    create_folders()
    images_list = os.listdir(data_folder)

    for image_name in tqdm(images_list):
        try:
            image_path = os.path.join(data_folder, image_name)
            image = cv2.imread(image_path)
        except Exception as e:
            print('Error processing image')
            continue

        image, status = blurry_filter(image)
        if status:
            save_path = os.path.join(save_folder, 'UNKNOWN', image_name)
            cv2.imwrite(save_path, image)
            continue

        image, status = yolo_v5_inference(image_path)
        image = blip_inference(image)

        if status == 'NOK':
            save_path = os.path.join(save_folder, 'NOK', image_name)
        else:
            save_path = os.path.join(save_folder, 'OK', image_name)

        image.save(save_path)
    logging.info('Process finished')


if __name__ == '__main__':
    main()
